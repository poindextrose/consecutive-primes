__author__ = 'Shawn Poindexter'

import sys

primes = [3, 5, 7, 11, 13, 17, 19, 23, 29, 31]

prime_sum_table = [[(x + y) in primes for y in range(19)] for x in range(19)]

saved_solutions = {}


def prime_necklace(N):
    if N % 2:
        return print(0)

    def count_solutions(n, remaining, last):
        if n == 1:
            return 1 if prime_sum_table[remaining[0]][1] and prime_sum_table[remaining[0]][last] else 0

        solution_id = tuple([last]) + tuple(remaining)
        if solution_id in saved_solutions:
            return saved_solutions[solution_id]

        n_solutions = 0
        for bead in [x for x in remaining if prime_sum_table[x][last]]:
            left_overs = remaining[:]
            left_overs.remove(bead)
            n_solutions += count_solutions(n - 1, left_overs, bead)

        saved_solutions[solution_id] = n_solutions
        return n_solutions

    all_numbers = list(range(2, N + 1))

    print(count_solutions(N - 1, all_numbers, 1))


test_cases = open(sys.argv[1], 'r')
for test in test_cases:
    # ignore test if it is an empty line
    if not test.strip():
        continue
    # 'test' represents the test case, do something with it
    prime_necklace(int(test))

test_cases.close()
